package dto;

public class Electrodomestico {

	private double precioBase;
	private String color;
	private char consumoEnergetico;	// Creamos los atributos
	private double peso;
	
	
	public Electrodomestico() {
		final double precioBase = 100;
		final String color = "blanco";
		final char consumoEnergetico = 'F';
		final double peso = 5;
		// Asignamos el valor default

		this.precioBase = precioBase;
		this.color = color;
		this.consumoEnergetico = consumoEnergetico;
		this.peso = peso;
		
		
		
		
		
	}


	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";// Hacemos el toString
	}


	public Electrodomestico(double precioBase, double peso) {
	
		this.precioBase = precioBase;
		this.peso = peso;
		this.consumoEnergetico = 'F';	 // Generamos el constructor
		this.color = "blanco";
	}


	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		
		this.precioBase = precioBase;
		this.color = comprobarColor(color); // Llamamos a la funcion
		this.consumoEnergetico = comprobarConsumo(consumoEnergetico); // Llamamos a la funicon
		this.peso = peso;
	}
	
	public static String comprobarColor(String color ) {
		
		if(color.equalsIgnoreCase("Blanco")||color.equalsIgnoreCase("Rojo")||color.equalsIgnoreCase("Negro")||color.equalsIgnoreCase("Azul")||color.equalsIgnoreCase("Gris") ) {
			return color;
		}// Si el color no es de los que se pide, devolvemos un default
		else {
			return "blanco";
		}
	}
	
	
	public static char comprobarConsumo(char consumo) {
        if (consumo == 'A' || consumo == 'B' ||  consumo == 'C' || consumo == 'D' || consumo == 'F') {
            return consumo;
        }else {// Si la energia no es de las que se pide, devolvemos un default
            return 'F';
        }
    }

	
	
	
	
	
	
}
