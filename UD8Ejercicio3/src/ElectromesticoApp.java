import dto.Electrodomestico;

public class ElectromesticoApp {

	public static void main(String[] args) {
		Electrodomestico electrodomestico1 = new Electrodomestico();
		System.out.println(electrodomestico1);
		Electrodomestico electrodomestico2 = new Electrodomestico(180.34,256.41);
		System.out.println(electrodomestico2);
		Electrodomestico electrodomestico3 = new Electrodomestico(190.34,"Verde",'A',500.42);
		System.out.println(electrodomestico3);

	}

}
